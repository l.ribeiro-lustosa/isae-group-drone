#include "opencv2/opencv.hpp"
#include "opencv2/aruco.hpp"
#include <iostream>

#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 

#define MATLAB_PORT    11112 
#define MAXLINE        1024 

cv::Vec3f rotationMatrixToEulerAngles(cv::Mat &R);

int main(int argc, char** argv) {

    cv::Mat cameraMatrix = ( cv::Mat_<double>(3,3) << 1280.1, 0.0, 531.592,
                                              0.0, 1295.53, 289.991,
                                              0.0, 0.0, 1.0 );

    cv::Mat distCoeffs = ( cv::Mat_<double>(1,5) << 0.233598, -4.2993, -0.0136352,  -0.0129543,
                                              23.0225  );

    int sockfd; 
    char buffer[MAXLINE]; 
    char hello[] = "x:123400000000;y:5678000000;z:123400000000;rx:123400000000;ry:5678000000;rz:123400000000;";
    struct sockaddr_in cliaddr; 

    // Creating socket file descriptor 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 

    memset(&cliaddr, 0, sizeof(cliaddr)); 

    // Filling cliaddr information 
    cliaddr.sin_family    = AF_INET; // IPv4 
    cliaddr.sin_addr.s_addr = INADDR_ANY; 
    cliaddr.sin_port = htons(MATLAB_PORT);

    cv::Mat frame;

    cv::VideoCapture cap("udp://0.0.0.0:11111",    cv::CAP_FFMPEG);
    //cap.set(cv::CAP_PROP_BUFFERSIZE, 3);

    cap >> frame; // get a first frame!

    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;
    cv::Ptr<cv::aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);

    std::vector<cv::Vec3d> rvecs, tvecs;
    cv::Vec3d eulerAngles{ 0, 0, 1 };
    cv::Mat R  = cv::Mat::eye(3,3,CV_32F);
    cv::Mat Ri = cv::Mat::eye(3,3,CV_32F);

    float markerLength = 10.9/100.0;

    for(;;) {

        cap >> frame;

        cv::aruco::detectMarkers(frame, dictionary, markerCorners, markerIds, parameters, rejectedCandidates);
        cv::aruco::drawDetectedMarkers(frame, markerCorners, markerIds);
        cv::aruco::estimatePoseSingleMarkers(markerCorners, markerLength, cameraMatrix, distCoeffs, rvecs, tvecs);

        cv::imshow("tello vision", frame);

        int minElementIndex = std::min_element(markerIds.begin(),markerIds.end()) - markerIds.begin();

        if (markerIds.size()>0) {
            cv::Rodrigues( 	rvecs[minElementIndex], R ); 
            cv::transpose(R,Ri);
            cv::Mat_<float> p_Ti_b_Ti(-Ri*tvecs[minElementIndex]);
            int target_x = static_cast<int>(100.0*tvecs[minElementIndex][0]);
            //int target_y = static_cast<int>(100.0*p_Ti_b_Ti(1));
            int target_y = static_cast<int>(100.0*tvecs[minElementIndex][1]);
            int target_z = static_cast<int>(100.0*tvecs[minElementIndex][2]);
            eulerAngles = rotationMatrixToEulerAngles(R);
            int target_rx = static_cast<int>(180.0/3.14159*eulerAngles[0]);
            int target_ry = static_cast<int>(180.0/3.14159*eulerAngles[1]);
            int target_rz = static_cast<int>(180.0/3.14159*eulerAngles[2]);
            sprintf(hello, "x:%d;y:%d;z:%d;rx:%d;ry:%d;rz:%d;\n", target_x, target_y, target_z, target_rx, target_ry, target_rz);
        } else {
            sprintf(hello, "x:%d;y:%d;z:%d;rx:%d;ry:%d;rz:%d;\n", 0, 0, 50, 0, 0, 0);
        }

        sendto(sockfd, (const char *)hello, strlen(hello),  
            0, (const struct sockaddr *) &cliaddr, 
            sizeof(cliaddr)); 

        if ( frame.empty() ) break; // end of video stream

        if( cv::waitKey(10) == 27 ) break; // stop capturing by pressing ESC

    }

    // the camera will be closed automatically upon exit
    cap.release();

    return 0;

}

// Calculates rotation matrix to euler angles
// The result is the same as MATLAB except the order
// of the euler angles ( x and z are swapped ).
cv::Vec3f rotationMatrixToEulerAngles(cv::Mat &R) {
 
    float sy = sqrt(R.at<double>(0,0) * R.at<double>(0,0) +  R.at<double>(1,0) * R.at<double>(1,0) );
 
    bool singular = sy < 1e-6; // If
 
    float x, y, z;
    if (!singular)
    {
        x = atan2(R.at<double>(2,1) , R.at<double>(2,2));
        y = atan2(-R.at<double>(2,0), sy);
        z = atan2(R.at<double>(1,0), R.at<double>(0,0));
    }
    else
    {
        x = atan2(-R.at<double>(1,2), R.at<double>(1,1));
        y = atan2(-R.at<double>(2,0), sy);
        z = 0;
    }
    return cv::Vec3f(x, y, z);
 
}
